package id.ub.kalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editText;
    Button bt, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9;
    Button bt10, bt11, bt12, bt13, bt14, bt15;

    public static double nilai = 0;
    public static double hasil = 0.0;
    public static String operasi = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    void init(){
        editText = (EditText)findViewById(R.id.et);
        bt1 = (Button)findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        bt2 = (Button)findViewById(R.id.bt2);
        bt2.setOnClickListener(this);
        bt3 = (Button)findViewById(R.id.bt3);
        bt3.setOnClickListener(this);
        bt4 = (Button)findViewById(R.id.bt4);
        bt4.setOnClickListener(this);
        bt5 = (Button)findViewById(R.id.bt5);
        bt5.setOnClickListener(this);
        bt6 = (Button)findViewById(R.id.bt6);
        bt6.setOnClickListener(this);
        bt7 = (Button)findViewById(R.id.bt7);
        bt7.setOnClickListener(this);
        bt8 = (Button)findViewById(R.id.bt8);
        bt8.setOnClickListener(this);
        bt9 = (Button)findViewById(R.id.bt9);
        bt9.setOnClickListener(this);
        bt = (Button)findViewById(R.id.bt);
        bt.setOnClickListener(this);

        bt10 = (Button)findViewById(R.id.bt10);
        bt10.setOnClickListener(this);
        bt11 = (Button)findViewById(R.id.bt11);
        bt11.setOnClickListener(this);
        bt12 = (Button)findViewById(R.id.bt12);
        bt12.setOnClickListener(this);
        bt13 = (Button)findViewById(R.id.bt13);
        bt13.setOnClickListener(this);
        bt14 = (Button)findViewById(R.id.bt14);
        bt14.setOnClickListener(this);
        bt15 = (Button)findViewById(R.id.bt15);
        bt15.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt:
                editText.setText(editText.getText().toString().trim()+"1");
                break;
            case R.id.bt1:
                editText.setText(editText.getText().toString().trim()+"2");
                break;
            case R.id.bt2:
                editText.setText(editText.getText().toString().trim()+"3");
                break;
            case R.id.bt3:
                editText.setText(editText.getText().toString().trim()+"4");
                break;
            case R.id.bt4:
                editText.setText(editText.getText().toString().trim()+"5");
                break;
            case R.id.bt5:
                editText.setText(editText.getText().toString().trim()+"6");
                break;
            case R.id.bt6:
                editText.setText(editText.getText().toString().trim()+"7");
                break;
            case R.id.bt7:
                editText.setText(editText.getText().toString().trim()+"8");
                break;
            case R.id.bt8:
                editText.setText(editText.getText().toString().trim()+"9");
                break;
            case R.id.bt9:
                editText.setText(editText.getText().toString().trim()+"0");
                break;

            case R.id.bt10:
                operasi = "tambah";
                nilai = Double.parseDouble(editText.getText().toString());
                editText.setText("");
                break;
            case R.id.bt11:
                operasi = "kali";
                nilai = Double.parseDouble(editText.getText().toString());
                editText.setText("");
                break;
            case R.id.bt13:
                operasi = "kurang";
                nilai = Double.parseDouble(editText.getText().toString());
                editText.setText("");
                break;
            case R.id.bt14:
                operasi = "bagi";
                editText.setText("");
                nilai = Double.parseDouble(editText.getText().toString());
                break;
            case R.id.bt12:
                if(operasi.equals("tambah")){
                    hasil = nilai + Double.parseDouble(editText.getText().toString().trim());
                }
                if(operasi.equals("kurang")){
                    hasil = nilai - Double.parseDouble(editText.getText().toString().trim());
                }
                if(operasi.equals("bagi")){
                    hasil = nilai / Double.parseDouble(editText.getText().toString().trim());
                }
                if(operasi.equals("kali")){
                    hasil = nilai * Double.parseDouble(editText.getText().toString().trim());
                }

                int nilaiTemp = (int) hasil;

                if(nilaiTemp == hasil){
                    editText.setText(String.valueOf((int) hasil));
                }else{
                    editText.setText(String.valueOf(hasil));
                }

                break;
            case R.id.bt15:
                nilai = 0;
                editText.setText("");
                break;
        }
    }
}
